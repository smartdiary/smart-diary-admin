appModule
.factory('$raysApi', function ($http, config, $q, $raysAdapter, $rootScope,$ionicHistory) {

    //============= Verify User =============================//
    function verifyUser(phNo, deviceInfo) {
        var deferred = $q.defer();
        var request = {
            api : config.apiUserUrl + 'verifyUser',
            data : {
                mobileNo : phNo,
                device_token : deviceInfo.device_token,
                device_id : deviceInfo.device_id,
                device_type : deviceInfo.device_type
            }
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

//================== Send OTP ========================== //
    function sendOtp(phNo) {
        var deferred = $q.defer();
        var request = {
            api : config.apiUserUrl + 'sendOtp',
            data : {
                mobileNo : phNo,
            }
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

  //=============== Verify OTP =============================== //

    function verifyOtp(phNo, otpCode, deviceInfo) {
        var deferred = $q.defer();
        var request = {
            api : config.apiUserUrl + 'verifyOtp',
            data : {
                mobileNo : phNo,
                OTPCode  : otpCode,
                device_token : deviceInfo.device_token,
                device_id : deviceInfo.device_id,
                device_type : deviceInfo.device_type
            }
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
//======================= Fetched User Details ==========================//
    function fetchDetails(uid) {
        var deferred = $q.defer();
        var request = {
            api : config.apiUserUrl + 'listShippingLoc',
            data : {
                userId : uid
            }
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
//======================== Select Category  ================================= //
      function selectCategory() {
          var deferred = $q.defer();
          var request = {
              api : config.apiMasterUrl + 'getSec',
              data : {
              }
          };
          $raysAdapter.postData(request).then(function(success) {
              deferred.resolve(success);
          }, function(error) {
              deferred.reject(error);
          });
          return deferred.promise;
      }
//========================== Get Product Items ================================//
        function productItems(id) {
            var deferred = $q.defer();
            var request = {
              api : config.apiMasterUrl + 'getProductCategoryList',
              data : {
                  secId: id
                }
           };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//========================== Get Product lists from each category ================================//
      function productItemLists(id) {
          var deferred = $q.defer();
          var request = {
              api : config.apiProductUrl + 'getProductList',
              data : {
                  catId: id
                  }
          };
          $raysAdapter.postData(request).then(function(success) {
              deferred.resolve(success);
          }, function(error) {
              deferred.reject(error);
          });
          return deferred.promise;
      }
//======================= Update Prfoile Information ==================================//
    function updateProfileInformation(uId, profileDetails) {
        var deferred = $q.defer();
        var request = {
            api : config.apiUserUrl + 'updateProfileDetail',
            data : {
                        userId          : uId,
                        name            : profileDetails.name,
                        dob             : profileDetails.dob,
                        email           : profileDetails.email,
                        state           : profileDetails.state,
                        mobile_no       : profileDetails.mobile_no,
                        city            : profileDetails.city,
                        profileImage    : profileDetails.profileImage,
                        gender          : profileDetails.gender,
                        anniversary     : profileDetails.anniversary
                }
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
//======================= Add Shipping Address ==================================//
            function addShipping(uId, shippingAddr) {
                var deferred = $q.defer();
                var request = {
                    api : config.apiUserUrl + 'addShippingLoc',
                    data : {
                          userId : uId,
                          streetNo : shippingAddr.streetNo,
                          buildingNo : shippingAddr.buildingNo,
                          city : shippingAddr.city,
                          landmark : shippingAddr.landmark,
                          pin_code : shippingAddr.pin_code,
                          state : shippingAddr.state
                        }
                };
                $raysAdapter.postData(request).then(function(success) {
                    deferred.resolve(success);
                }, function(error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }
//======================= Update Shipping Address ==================================//
            function updateShipping(uId, shippingAddr) {
                var deferred = $q.defer();
                var request = {
                    api : config.apiUserUrl + 'updateShippingLoc',
                    data : {
                            addressId : shippingAddr.addressId,
                            userId  : uId,
                            buildingNo : shippingAddr.buildingNo,
                            streetNo : shippingAddr.streetNo,
                            city : shippingAddr.city,
                            landmark : shippingAddr.landmark,
                            pin_code : shippingAddr.pin_code,
                            state : shippingAddr.state
                        }
                };
                $raysAdapter.postData(request).then(function(success) {
                    deferred.resolve(success);
                }, function(error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }
//=================== Delete Shipping Address ============================== //
        function deleteShipping(uId, shpId) {
            var deferred = $q.defer();
            var request = {
                api : config.apiUserUrl + 'delShippingLoc',
                data : {
                        shippingId : shpId,
                        userId  : uId
                    }
            };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//========================= Get Image Lists =================================== //
        function getImgLists(uId) {
            var deferred = $q.defer();
            var request = {
                api : config.apiMasterUrl + 'listGallery',
                data : {
                        userId  : uId
                    }
            };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//========================== Delete Images ==================================== //
        function deleteImages(uId, gId) {
            var delId = gId.join();
            var deferred = $q.defer();
            var request = {
                api : config.apiMasterUrl + 'deleteGallery',
                data : {
                        userId  : uId,
                        galleryId : delId
                    }
            };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

//========================== view cart list ==================================== //
        function viewCartList(obj) {
            var deferred = $q.defer();
            var request = {
                api :  config.apiOrdersUrl + 'myCartList',
                data : {
                        userId : obj
                    }
            };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//========================== delete from cart list ==================================== //
        function deleteFromCartList(uId,cartid) {
            var deferred = $q.defer();
            var request = {
                api : config.apiOrdersUrl + 'deleteCart',
                data : {
                    userId  : uId,
                    itemId  : cartid
                    }
            };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//========================== view order list ==================================== //
        function viewOrderList(obj,status) {
            var deferred = $q.defer();
            var request = {
                api :  config.apiOrdersUrl + 'orderList',
                data : {
                        userId : obj,
                        orderStatus : status
                    }
            };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//========================== view order detail ==================================== //
            function viewOrderDetail(id,viewid) {
                var deferred = $q.defer();
                var request = {
                    api :  config.apiOrdersUrl + 'orderDetailList',
                    data : {
                            userId : id,
                            orderNo : viewid
                        }
                };
                $raysAdapter.postData(request).then(function(success) {
                    deferred.resolve(success);
                }, function(error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }

//========================== delete from order list ==================================== //
        function cancelFromOrderList(uId,orderNo) {
            var deferred = $q.defer();
            var request = {
                api : config.apiOrdersUrl + 'cancelOrder',
                data : {
                    userId  : uId,
                    orderNo : orderNo
                    }
            };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//=================== Send Order Before Payment ======================= //
        function sendOrder(obj) {
            var deferred = $q.defer();
            var request = {
                api     : config.apiOrdersUrl + 'sendOrder',
                data    : obj
            };
            $raysAdapter.postDataJson(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//====================== add to Cart ================================ //
        function addIntoCart(product, templateInfo) {
            var deferred = $q.defer();
            var request = {
                api     : config.apiOrdersUrl + 'addToCartPrint',
                data    : {
                            productType             : product.type,
                            productImagepath        : product.path,
                            productInfo             : product.info,
                            templateInfo            : templateInfo
                    }
            };
            $raysAdapter.postDataJson(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//==================== Add to Cart for Calendar =========================== //
        function addCalendarToCart(product, templateInfo) {
            var deferred = $q.defer();
            var request = {
                api     : config.apiOrdersUrl + 'addToCart',
                data    : {
                            productType             : product.type,
                            productImagepath        : product.path,
                            productInfo             : product.info,
                            templateInfo            : templateInfo
                    }
            };

            $raysAdapter.postDataJson(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
// //================== get image size ================================//
//         function getImageSize(url) {
//             var deferred = $q.defer();
//             var img = new Image();
//             img.src = url;
//             img.onload = function() {
//                 var imageDetails = {
//                     width : img.width,
//                     height : img.height
//                 }
//                 deferred.resolve(imageDetails);
//             }
//             return deferred.promise;
//         }
//========================== confirmOrder =============================== //
        function orderConfirmation(uId, orderNo) {
            var deferred = $q.defer();
            var request  = {
                api     : config.apiOrdersUrl + 'confirmOrder',
                data    : {
                            userId      : uId,
                            orderNo     : orderNo,
                        }
            };
            $raysAdapter.postData(request).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
//====================== File submit url ======================== //
    function fileSubmitUrl() {
        var url;
        if ($ionicHistory.currentView().stateName == "app.products-design") {
            url = urlGenerator('addVehiclePUCApi');
        }
        return url;
    }
//====================== Apply Discount =============================== //
    function applyTheDiscount( disCountObj) {
        var deferred = $q.defer();
        var request  = {
            api     : config.apiOrdersUrl + 'applyCoupon',
            data    : disCountObj
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
//====================== Remove Coupon ================================== //
    function removeCoupon( removeObj) {
        var deferred = $q.defer();
        var request  = {
            api     : config.apiOrdersUrl + 'removeCoupon',
            data    : removeObj
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
//============================ Price Calculation ============================= //
    function calculatePrice(priceObj) {
        var deferrd = $q.defer();
        var request  = {
            api     : config.apiProductUrl + 'getProductCourierPrice',
            data    : priceObj
        };
        $raysAdapter.postData(request).then(function(success) {
            deferrd.resolve(success);
        }, function(error) {
            deferrd.reject(error);
        });
        return deferrd.promise;
    }
//=========================== Get coupon Details ============================== //
    function getCouponLists() {
        var deferred = $q.defer();
        var request  = {
            api     : config.apiOrdersUrl + 'couponList'
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
//=========================== Get coupon Details ============================== //
    function getTemplateDetails(paramId) {
        var deferred = $q.defer();
        var request  = {
            api         : config.apiProductUrl + 'getPositionTemplate',
            data        : {
                            productId : paramId
                        }
        };
        $raysAdapter.postData(request).then(function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    return  {
        verifyUser                  : verifyUser,
        sendOtp                     : sendOtp,
        verifyOtp                   : verifyOtp,
        selectCategory              : selectCategory,
        productItems                : productItems,
        productItemLists            : productItemLists,
        updateProfileInformation    : updateProfileInformation,
        addShipping                 : addShipping,
        deleteShipping              : deleteShipping,
        fetchDetails                : fetchDetails,
        getImgLists                 : getImgLists,
        deleteImages                : deleteImages,
        viewCartList                : viewCartList,
        updateShipping              : updateShipping,
        deleteFromCartList          : deleteFromCartList,
        viewOrderList               : viewOrderList,
        sendOrder                   : sendOrder,
        cancelFromOrderList         : cancelFromOrderList,
        orderConfirmation           : orderConfirmation,
        viewOrderDetail             : viewOrderDetail,
        addIntoCart                 : addIntoCart,
        //applyCartCoupon             : applyCartCoupon,
        fileSubmitUrl               : fileSubmitUrl,
        addCalendarToCart           : addCalendarToCart,
        applyTheDiscount            : applyTheDiscount,
        calculatePrice              : calculatePrice,
        removeCoupon                : removeCoupon,
        getCouponLists              : getCouponLists,
        getTemplateDetails          : getTemplateDetails
    };
});
