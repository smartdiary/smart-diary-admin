/**
 * This is the service that communicates with Rays Images Serve. It is kind of
 * a state machine (not enforced though) in the sense that methods
 * must be called sequentially.
 *
 * This is implemented as a service, therefore, as per AngularJS, this is a
 * singleton.
 * This script will not come under strict made: as we are accessing 'caller', 'callee', and 'arguments' properties
 */
appModule
.factory('$raysAdapter', function($http, $q, config, $timeout) {

    function postData(reqApiObj) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.apiBaseUrl + reqApiObj.api,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: reqApiObj.data
        }).success(function (res) {
            deferred.resolve(res);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    function postDataJson(reqApiObj) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: config.apiBaseUrl + reqApiObj.api,
            headers: {'Content-Type': 'application/json'},
            data: reqApiObj.data
        }).success(function (res) {
            deferred.resolve(res);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }


     /*
      * Expose the public function
      */
 	return {
        postData : postData,
        postDataJson : postDataJson
 	};
 });
