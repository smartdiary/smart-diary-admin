'use strict';

/**
 * SQL Adapter
 * Middle layer / Database Layer for the storage system
 * @written by Krishnendu Sekhar Das
 * @modify by Krishnendu Sekhar Das
 * Written on 06-08-2015
 */
 appModule.factory('$sqlAdapter', function($q, $log, $rootScope, $cordovaSQLite, $timeout) {
    //Schema Generator
    function createSchema(db, tableList) {
        var deferred    = $q.defer();
        var counter     = 0;
        db.transaction(function (tx) {
            angular.forEach(tableList, function (columnName) {
                tx.executeSql(columnName, null, function (tx, result) {
                    counter++;
                    if(counter == tableList.length) {
                        deferred.resolve(result);
                    }
                }, function (transaction, error) {
                    deferred.reject(error);
                });
            });
        });
        return deferred.promise;
    }

    //Delete Schema
    function deleteSchema(db, tableList) {
        var deferred    = $q.defer();
        var counter     = 0;
        db.transaction(function (tx) {
            angular.forEach(tableList, function (columnName) {
                tx.executeSql(columnName, null, function (tx, result) {
                    counter++;
                    if(counter == tableList.length) {
                        deferred.resolve(result);
                    }
                }, function (transaction, error) {
                    deferred.reject(error);
                });
            });
        });
        return deferred.promise;
    }

    //Prepare Statement to insert single record
    function sqlSaveOne(db, tableName, columnList, dataLists) {
        var deferred    = $q.defer();
        var query = '';
        query   += "INSERT INTO "+tableName;
        query   += " ( ";
        query   += columnList.toString();
        query   += " ) VALUES ";
        var data    = [];
        var rowArgs    = [];
        var argumentArr = [];
        var argumentStr = '';
        for(var i=0;i < columnList.length;i++) {
           argumentArr.push('?');
        }
        argumentStr = "( "+argumentArr.toString()+" )";
        rowArgs.push(argumentStr);
        angular.forEach(columnList, function(columnName, key) {
           data.push(dataLists[columnName]);
        });
        query += rowArgs.join(", ");
        //console.log('query',query);
        $cordovaSQLite.execute(db, query, data).then(function (res) {
           deferred.resolve("inserted successfully");
        }, function (err) {
           deferred.reject(err);
        });
        return deferred.promise;
    }

//====================== Select all data from SQLite =========================//
    function sqlSelectAll(db, tableName, columnList) {
        var deferred        = $q.defer();
        var columnListStr   = '';
        if(columnList.length > 0){
            columnListStr   = columnList.toString();
        } else {
            columnListStr   = ' * ';
        }
        var query = "SELECT " + columnListStr + " FROM " + tableName;
        $cordovaSQLite.execute(db, query).then(function(res) {
            var response = {};
            var dataDetails = [];
            var i = 0;
            if(res.rows.length > 0) {
                for( i = 0; i<res.rows.length ; i++) {
                    dataDetails.push(res.rows.item(i));
                }
            } else {
            }

            response.data = dataDetails;
            deferred.resolve(response.data);
        }, function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    //======================= Update Table =============================== //
        function sqlUpdateTable(db, tableName, updateList, conditionArr, cdatatype, udatatype) {
            var deferred        = $q.defer();
            var updateColumnArr  = [];
            var updateQueryStr   = '';
            var conditionColumnArr  = [];
            var queryStr            = '';
            if(udatatype == "INTEGER") {
                angular.forEach(updateList, function(columnValue, columnName) {
                    var updateStr = columnName + " = " + columnValue;
                    updateColumnArr.push(updateStr);
                });
            } else {
                 angular.forEach(updateList, function(columnValue, columnName) {
                    var index = -1;
                    if(angular.isString(columnValue)) {
                      index = columnValue.indexOf('"');
                    }
                    var updateStr;
                    if(index > -1) {
                        updateStr = columnName + " = '" + columnValue + "'";
                    } else {
                        updateStr = columnName + ' = "' + columnValue + '"';
                    }
                    updateColumnArr.push(updateStr);
                });
            }
            updateQueryStr += updateColumnArr.join(" , ");
            if(cdatatype == "INTEGER") {
                angular.forEach(conditionArr, function(columnValue, columnName) {
                    var conditionStr = columnName + " = " + columnValue;
                    conditionColumnArr.push(conditionStr);
                });
            } else {
                angular.forEach(conditionArr, function(columnValue, columnName) {
                    var conditionStr = columnName + ' = "' + columnValue +'"';
                    conditionColumnArr.push(conditionStr);
                });
            }
            queryStr += conditionColumnArr.join(" AND ");
            var query = " UPDATE " + tableName + " SET " + updateQueryStr + " WHERE " + queryStr;
            $cordovaSQLite.execute(db, query).then(function(res) {
                if(res.rows.length > 0) {
                    //console.log("SELECTED -> " + JSON.stringify(res.rows.item(0)));
                } else {
                    //console.log("No results found");
                }
                deferred.resolve(res.rows.item(0));
            }, function (err) {
                deferred.reject(noRecord);
            });
            return deferred.promise;
        }
//============================ Delete Data ====================================//

            function sqlDeleteByCondition(db, tableName, conditionArr) {
                var deferred    = $q.defer();
                var conditionColumnArr  = [];
                var queryStr            = '';
                angular.forEach(conditionArr, function(columnValue, columnName) {
                    var conditionStr = columnName + " = '" + columnValue + "'";
                    conditionColumnArr.push(conditionStr);
                });
                queryStr += conditionColumnArr.join(" AND ");
                var query = " DELETE FROM " + tableName + " WHERE " + queryStr;
                $cordovaSQLite.execute(db, query).then(function (res) {
                   deferred.resolve("Data cleaned successfully");
                }, function (err) {
                   deferred.reject(err);
                });
                return deferred.promise;
            }

    /*
     * Expose the public function
     */
 	return {
        createSchema            :   createSchema,
        deleteSchema            :   deleteSchema,
        sqlSaveOne              :   sqlSaveOne,
        sqlSelectAll            :   sqlSelectAll,
        sqlDeleteByCondition    :   sqlDeleteByCondition,
        sqlUpdateTable          :   sqlUpdateTable



 	};
 });
