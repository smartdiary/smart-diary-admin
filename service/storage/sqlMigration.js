'use strict';

/**
 * SQL Migration
 * Middle layer / Database Layer for the storage system
 * @written by Krishnendu Sekhar Das
 * @modify by Krishnendu Sekhar Das
 * Written on 01-02-2016
 */
appModule.factory('$sqlMigration', function($q, $log, $timeout, localStorageService, config, $sqlAdapter, $cordovaSQLite) {
    var db                  = '';
    var dbVersionShould     = config.dbCurrentVersion;
    var dbCurrentVersion    = 0;
    var deferObject         = [];
    var dbLocalStorage      = {
                                key : "dbVersion",
                                value : 0
                            };
    var dbTables            = {
                                shippingAddressTable:'SHIPPING_ADDRESS'
                            };

    function checkDbMigration() {
        if(deferObject['checkDbMigration'] == null){
            var deferred = $q.defer();
            deferObject['checkDbMigration'] = deferred;
        } else {
            deferred = deferObject['checkDbMigration'];
        }
        dbCurrentVersion = localStorageService.get(dbLocalStorage.key);
        if(localStorageService.get(dbLocalStorage.key)) {
            dbCurrentVersion = localStorageService.get(dbLocalStorage.key);
        }else {
            dbCurrentVersion = 0;
        }
        if(dbCurrentVersion == 0) {
            // Latest schema | db version null -> 1
            var tableList = [];
            tableList.push("CREATE TABLE IF NOT EXISTS "+dbTables.shippingAddressTable+" ( addressId integer primary key, buildingNo VARCHAR(50), streetNo VARCHAR(50), city VARCHAR(50), state VARCHAR(50), pin_code integer, landmark VARCHAR(50))");
            $sqlAdapter.createSchema(db, tableList).then(function(res) {
                localStorageService.set(dbLocalStorage.key, config.dbCurrentVersion);
                // recheck Db Migration
                checkDbMigration();
            }, function() {
                deferObject['checkDbMigration'] = null;
                deferred.reject("schema creation error");
            });
        }/*if(dbCurrentVersion == 1) {
            localStorageService.set(dbLocalStorage.key, dbCurrentVersion+1);
            console.log("------------------------db migration | COMPLETED to 1---------------------------");
            checkDbMigration();
        }if(dbCurrentVersion == 2) {
            localStorageService.set(dbLocalStorage.key, dbCurrentVersion+1);
            console.log("------------------------db migration | COMPLETED to 2---------------------------");
            checkDbMigration();
        }*/else {
            if(dbCurrentVersion == config.dbCurrentVersion) {
                // db migration | COMPLETED
                deferObject['checkDbMigration'] = null;
                deferred.resolve();
            }else {
                // db migration | ERROR
                deferObject['checkDbMigration'] = null;
                deferred.reject("schema creation error");
            }
        }
        return deferred.promise;
    }

    function openDB() {
        var options = {
            name : "rays-imaging.db",
            location : "default"
        };
        db = $cordovaSQLite.openDB(options);
        return db;
    }

    function getTables() {
        return dbTables;
    }

    function getCurrentVerDB() {
        return localStorageService.get(dbLocalStorage.key);
    }

    /*
     * Expose the public function
     */
    return {
        openDB : openDB,
        getTables : getTables,
        checkDbMigration : checkDbMigration,
        getCurrentVerDB : getCurrentVerDB
    };
});
