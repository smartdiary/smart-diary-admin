'use strict';

/**
 * Storage system
 * This is implemented as a service, therefore, as per AngularJS, this is a
 * singleton.
 */
 appModule.factory('$raysLocalStorage', function($q, $log, $rootScope, $interval, localStorageService, $timeout, $cordovaSQLite, $sqlAdapter, $sqlMigration) {
    var db                  = '';
    var dbTables            = {};
    var noRecord            = 'No result found';
    var unableTosave        = 'Unable to save data in storage';
    var createdTableList    = [];

    /*
     * This will be fired at the time of Device ready to create database and required table for the app
     */
    function init() {
        var deferred    = $q.defer();
        db = $sqlMigration.openDB();
        dbTables = $sqlMigration.getTables();
        $sqlMigration.checkDbMigration().then(function(success) {
            deferred.resolve(success);
        },function(error) {
            deferred.reject("schema creation error");
        });
        return deferred.promise;
    }

    function initRepeat() {
        db = $sqlMigration.openDB();
        dbTables = $sqlMigration.getTables();
    }

    function getRequireObj() {
        return {
            'db' : db,
            'tables' : dbTables
        };
    }

    //Setter & getter Storage Management - Common Access
    function setStorage(storageKeyName, storageKeyValue) {
        localStorageService.set(storageKeyName, storageKeyValue);
    }

    function getStorage(storageKeyName) {
        return localStorageService.get(storageKeyName);
    }

    function removeStorage(storageKeyName) {
        return localStorageService.remove(storageKeyName);
    }

    //Setter & getter Storage Management - Global and rootLevel Access
    function setGlobalStorage(storageKeyName, storageKeyValue) {
        localStorageService.set(storageKeyName, storageKeyValue);
        if(storageKeyName == 'userInfo') {
            $rootScope.userInfo = storageKeyValue;
        }
    }

    function getGlobalStorage(storageKeyName) {
        if(storageKeyName == 'userInfo') {
            var userInfo = localStorageService.get('userInfo');
            $rootScope.userInfo = userInfo;
            return $rootScope.userInfo;
        }
    }

    function removeGlobalStorage(storageKeyName) {
        localStorageService.remove(storageKeyName);
        $rootScope.userInfo = {};
    }

    function updateGlobalStorage(storageKeyName, keyName, status) {
        var storageDetails = localStorageService.get(storageKeyName);
        storageDetails[keyName] = status;
        localStorageService.set(storageKeyName, storageDetails);
        if(storageKeyName == 'userInfo') {
            $rootScope.userInfo = storageDetails;
        }
    }

    //===================== Insert Shipping Addresses  ============================ //
        function insertShippingAddress (address) {
            var deferred = $q.defer();
            var tableName = dbTables.shippingAddressTable;
            var columnList = ['addressId', 'buildingNo', 'streetNo', 'city', 'state', 'pin_code', 'landmark'];
            $sqlAdapter.sqlSaveOne(db, tableName, columnList, address).then(function(records) {
                //Data saved successfully
                deferred.resolve(records);
            },function(){
                //Unable to save data in database - localStorage needs to work here
                deferred.reject(noRecord);
            });
            return deferred.promise;
        }
    //=========================== Select All Shipping Addresses =========================== //
        function selectAllShippingAddress () {
            var deferred = $q.defer();
            var tableName = dbTables.shippingAddressTable;
            var columnList = [];
            $sqlAdapter.sqlSelectAll(db, tableName, columnList).then(function(records) {
                //Data saved successfully
                deferred.resolve(records);
            },function(){
                //Unable to save data in database - localStorage needs to work here
                deferred.reject(noRecord);
            });
            return deferred.promise;
        }
    //================================ Delete Shipping Address ================================ //
        function deleteShippingAddress (id) {
            var deferred = $q.defer();
            var tableName = dbTables.shippingAddressTable;
            var conditionArr = {"addressId" : id};
            $sqlAdapter.sqlDeleteByCondition(db, tableName, conditionArr).then(function(records) {
                //Data saved successfully
                deferred.resolve(records);
            },function(){
                //Unable to save data in database - localStorage needs to work here
                deferred.reject(noRecord);
            });
            return deferred.promise;
        }

    //============================= Update Shipping Address ========================== //
        function updateShippingAddress (address) {
            var deferred = $q.defer();
            var tableName = dbTables.shippingAddressTable;
            var updateList = {'buildingNo' : address.buildingNo , 'streetNo' : address.streetNo, 'city' : address.city, 'state' : address.state, 'pin_code' : address.pin_code , 'landmark' : address.landmark};
            var conditionArr = {"addressId" : address.addressId};
            $sqlAdapter.sqlUpdateTable(db, tableName, updateList, conditionArr, "INTEGER", "STRING").then(function(records){
                //Data saved successfully
                deferred.resolve(records);
            },function(){
                //Unable to save data in database - localStorage needs to work here
                deferred.reject(noRecord);
            });
            return deferred.promise;
        }


 	return {
        init            :   init,
        initRepeat      :   initRepeat,
        getRequireObj   :   getRequireObj,
        setStorage      :   setStorage,
        getStorage      :   getStorage,
        setGlobalStorage : setGlobalStorage,
        getGlobalStorage    : getGlobalStorage,
        insertShippingAddress : insertShippingAddress,
        selectAllShippingAddress : selectAllShippingAddress,
        deleteShippingAddress   : deleteShippingAddress,
        updateShippingAddress   : updateShippingAddress,
        removeStorage           : removeStorage

    };
 });
