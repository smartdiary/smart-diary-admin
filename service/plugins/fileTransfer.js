appModule
//=============================== Profile Upload ============================================== //
    .factory('$rayfileTransfer', function ($cordovaFileTransfer, $q, config) {
        function uploadFile(uID, uInfo) {
            var server      = config.apiBaseUrl + 'Users/updateProfile';
            var filePath    = uInfo.profileImage;
            var params      = {
                        userId      : uID,
                        name        : uInfo.name,
                        dob         : uInfo.dob,
                        email       : uInfo.email,
                        state       : uInfo.state,
                        city        : uInfo.city,
                        mobile_no   : uInfo.mobile_no,
                        gender      : uInfo.gender,
                        anniversary : uInfo.anniversary
            }


            var options     = {
                        fileName    : filePath.substr(filePath.lastIndexOf('/')+1),
                        mimeType    : "image/png",
                        fileKey     : "profileImg",
                        params      : params
            }
            var deferred = $q.defer();
            $cordovaFileTransfer.upload(server, filePath, options).then(function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            }, function (progress) {
                deferred.notify(progress);
            });
            return deferred.promise;
        }
        return  {
            uploadFile : uploadFile
        };
    })
