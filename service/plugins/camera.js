appModule
//============================= Service for toast ===============================//
.factory('$camera', function($cordovaImagePicker, $q, $cordovaCamera){
    function getImgPaths(qty) {
        var deferred = $q.defer();
        var options = {
              maximumImagesCount: qty,
              width             : 0,
              height            : 0,
              quality           : 100
        };
        $cordovaImagePicker.getPictures(options).then(function (results) {
            deferred.resolve(results);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    function getCameraPath() {
        var deferred = $q.defer();
        var options = {
              quality                   : 100,
              destinationType           : Camera.DestinationType.FILE_URI,
              sourceType                : Camera.PictureSourceType.CAMERA,
              encodingType              : Camera.EncodingType.JPEG,
              popoverOptions            : CameraPopoverOptions,
              correctOrientation        : true,
              saveToPhotoAlbum          : true
        };
        $cordovaCamera.getPicture(options).then(function(results) {
            deferred.resolve(results);
          }, function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    return  {
        getImgPaths : getImgPaths,
        getCameraPath : getCameraPath
    };
});
