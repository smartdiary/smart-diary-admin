appModule
//=============================== preloader display ============================================== //
.service('$raysLoader', function ($ionicLoading) {
    this.busyOn = function (message) {
      $ionicLoading.show({
        template : '<ion-spinner icon="dots"></ion-spinner><p>'+ message +'</p>'
      }).then(function(){
         console.log("The loading indicator is now displayed");
       });
    }
    this.busyOff = function () {
        $ionicLoading.hide();
    }
})
