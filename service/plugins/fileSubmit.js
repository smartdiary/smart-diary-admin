
'use strict';
appModule.factory('FileUploadService',
  ['$rootScope','$q','$raysApi', '$cordovaFileTransfer',
    function ($rootScope, $q, $raysApi, $cordovaFileTransfer ) {
        var deferred;
        $rootScope.completedPercentage = 0;
        $rootScope.perCounter          = 0;
        return {
            fileUpload : function(fileURL, obj, length, fileName){

                deferred                = $q.defer();
                var options             = new FileUploadOptions();
                options.fileKey         = fileName ? fileName : "image";
                options.chunkedMode     = false;
                options.fileName        = 'documentPicked.jpg';
                options.mimeType        = "image/jpeg"; // make this dynamic
                options.httpMethod      = "POST";
                options.params          = obj;

                console.log(options);
                $cordovaFileTransfer.upload($raysApi.fileSubmitUrl(), fileURL, options).then(
                    function(result) {
                        $rootScope.perCounter += 100;
                        deferred.resolve(result.response);
                },  function(err) {
                        console.log("ERROR: " + JSON.stringify(err));
                        deferred.reject(err);
                },  function (progress) {
                    console.log(progress)
                    if (progress.lengthComputable) {
                        deferred.notify(Math.round((progress.loaded / progress.total)*100));
                    }
                    // var total           = progress.total;
                    // var loaded          = progress.loaded;
                    // var totalPerc       = Math.round((loaded/total)* 100);
                    // $rootScope.completedPercentage = Math.round(($rootScope.perCounter + totalPerc)/length);
                    // $rootScope.$broadcast('addProgressImage', {persent: $rootScope.completedPercentage});
                    // console.log('global % = ' + $rootScope.completedPercentage);
                });
                return deferred.promise;
            },
        };
    }
]);
