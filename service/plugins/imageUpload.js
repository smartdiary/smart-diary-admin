appModule
//=============================== Profile Upload ============================================== //
.factory('$raysImageUpload', function ($cordovaFileTransfer, $q, config) {

//================= Image Upload in Server ==============================//
    function uploadImage(uID, link) {
        var server      = config.apiBaseUrl + 'masters/galleryupload';
        var filePath    = link;
        var params      = {
                    userId : uID
        }
        var options     = {
                fileName    : filePath.substr(filePath.lastIndexOf('/')+1),
                mimeType    : "image/png",
                fileKey     : "file",
                params      : params
        }
        var deferred = $q.defer();
        $cordovaFileTransfer.upload(server, filePath, options).then(function(result) {
            deferred.resolve(result);
        }, function(err) {
            deferred.reject(err);
        }, function (progress) {
            if (progress.lengthComputable) {
                deferred.notify(Math.round((progress.loaded / progress.total)*100));
            }
        });
        return deferred.promise;
    }
//========================= Image Update in Server ============================ //

    function updateImage(uID, galId, link) {
        var server      = config.apiBaseUrl + 'masters/galleryupdate';
        var filePath    = link;
        var params      = {
                userId      : uID,
                galleryId   : galId
        }
        var options     = {
                fileName    : filePath.substr(filePath.lastIndexOf('/')+1),
                mimeType    : "image/png",
                fileKey     : "file",
                params      : params
        }
        var deferred = $q.defer();
        $cordovaFileTransfer.upload(server, filePath, options).then(function(result) {
            deferred.resolve(result);
        }, function(err) {
            deferred.reject(err);
        }, function (progress) {
            deferred.notify(progress);
        });
        return deferred.promise;
    }
    function uploadBackgroundImage(link,obj,server) {
        var server      = server;
        var filePath    = link;
        var params      = obj
        var options     = {
                fileName    : filePath.substr(filePath.lastIndexOf('/')+1),
                mimeType    : "image/png",
                fileKey     : "file",
                params      : params
        }
        var deferred = $q.defer();
        $cordovaFileTransfer.upload(server, filePath, options).then(function(result) {
            deferred.resolve(result);
        }, function(err) {
            deferred.reject(err);
        }, function (progress) {
            //console.log(progress);
            if (progress.lengthComputable) {
                deferred.notify(progress);
                //deferred.notify(Math.round((progress.loaded / progress.total)*100));
            }
        });
        return deferred.promise;
    }

    return {
        uploadImage : uploadImage,
        updateImage : updateImage,
        uploadBackgroundImage : uploadBackgroundImage
    }

  })
