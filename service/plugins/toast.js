appModule
//============================= Service for toast ===============================//
.service('$toastMessage', function($cordovaToast){
    this.show  = function (message) {
        $cordovaToast.show(message, "short", "bottom");
    }
})
