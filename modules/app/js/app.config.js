"use strict";
/**
 *
 * constant for main module
 *
*/
appModule
.constant("ENV", {
    dbCurrentVersion            : 1,
    httpTimeout                 : 30000,
    apiserverRoot               : "http://150.129.179.174:5000",
    apiUserUrl                  : "front/unauthUser",
    apiMasterUrl                : "masters/",
    apiProductUrl               : "products/",
    apiOrdersUrl                : "Orders/"
})
.constant("message", {
    networkErrorMsg            : "Internet is not available.",
    errorMsg                   : 'Unable to process your request. Apologies for the inconvenience. You may try again later.',
    invalidNo                  : "Please enter valid 10 digit mobile number",
    backMsg                    : "Press back button again to exit.",
    uploadFail                 : "Upload Failed",
    imgUploadSuccess           : "Image uploaded successfully"
});


