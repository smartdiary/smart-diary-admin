appModule
.controller('appCtrl', function ($scope, $state, localStorageService, $location){
	var userId      = localStorageService.get('userId');
    var userName    = localStorageService.get('userName');
    $scope.userName = userName;

    $scope.goTo = function(){
    	//Session.clear();
        var userId      	= localStorageService.set('userId', null);
        var userName      	= localStorageService.set('userName', null);
        var userEmail      	= localStorageService.set('userEmail', null);
        var userMobile      = localStorageService.set('userMobile', null);
        var companyName     = localStorageService.set('companyName', null);
        var companyAddress 	= localStorageService.set('companyAddress', null);
        var userType      	= localStorageService.set('userType', null);
        $state.go('login.registration');
    }
});
