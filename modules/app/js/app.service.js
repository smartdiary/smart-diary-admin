appModule
//=============================== Factories ============================================== //
.factory('$raysService', function ($ionicLoading, config, $ionicPopup, $raysLocalStorage, $http, $q, $ionicModal, $raysApi, $toastMessage) {
    var object = {};

//=============== Busy on Function ================================//
    function busyOn(message) {
        $ionicLoading.show({
          template : '<ion-spinner icon="dots"></ion-spinner><p>'+ message +'</p>',
          hideOnStageChange: true
          //duration: config.httpTimeout
        });
    }
//=============== Busy off Function ================================//
    function busyOff() {
        $ionicLoading.hide();
    }
//=============== show infomation in pop Function ================================//
    function showInfo(message) {
        var myPopup = $ionicPopup.show({
            title: 'Alert',
            template : message,
            cssClass    : 'customPopup',
            buttons: [{
                    text: 'OK',
                    type: 'button-dark button-small',
                    onTap: function(e) {
                         myPopup.close();
                    }
                },
            ]
        });
        myPopup.then(function(res) {
            myPopup.close();
        });
    }
//========================= Set the value =================================//
        function setData(key, val){
            object[key] = val;
        }
//====================== Get the Value ==================================//
        function getData(key){
            return object[key];
        }

// =================== Email Validation====================================//
     function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

//===================== Special Character Validation ======================//
      function validateSpecialChar(inputData) {
        var re = /^[a-zA-Z0-9- ]*$/;
        return re.test(inputData);
    }

//=====================  Phone No Validation ================================//
      function mobileValidate(mobile) {
        var mobno = /^\d{10}$/;
        return mobno.test(mobile);
    }
// //================= function Quantity Validation ====================== //
//     function noValidation(number) {
//         var no = ^[1-9]\d*(\.\d+)?$;
//         return no.test(number);
//     }

//==================  Pin Code Validation =====================================//
        function pinValidate(pin) {
            var pinno = /^\d{6}$/;
            return pinno.test(pin);
        }
//==================== Check Network Connection ==============================//
        function checkConnection() {
            if(navigator && navigator.connection && navigator.connection.type === 'none') {
                return false;
            }
            return true;
        }
//======================= Login Mode or not ================================//
         function hasUserId() {
             var userInfo = $raysLocalStorage.getStorage('userInformation');
             if(userInfo){
                 return true;
             } else{
                 return false;
             }
        }
//======================= JSON Calling For State Lists ======================//
    function getStates() {
            var deferred = $q.defer();
            $http.get('json/states.json').success(function (response, status, headers, config) {
                if(status == 200) {
                    deferred.resolve(response);
                } else {
                    deferred.reject(response);
                }
            }).error(function (response, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
    }
//======================= JSON Calling For Gender Lists ======================//
    function getGender() {
            var deferred = $q.defer();
            $http.get('json/gender.json').success(function (response, status, headers, config) {
                if(status == 200) {
                    deferred.resolve(response);
                } else {
                    deferred.reject(response);
                }
            }).error(function (response, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
    }
//==================== Open Modal For address ============================= //
        function openModal(url, $scope, cb) {
            var openModal = $ionicModal.fromTemplateUrl(url, {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                cb(modal);
            });

            //return openModal;
        }
//==================== Add and  Update address ============================= //
        function updateShippingAddress(userId,type,address,cb)  {
            if(type == "Add Address") {
                busyOn('Updating Profile');
                $raysApi.addShipping(userId, address).then(function(result) {
                    if(result.responseCode == 200) {
                        addSqlite(result.addressParam);
                        busyOff();
                        cb(true);
                    } else if(result.responseCode == 201) {
                        busyOff();
                        $toastMessage.show(result.responseMessage);
                        cb(false);
                    }
                }, function(error){
                    $toastMessage.show(message.errorMsg);
                    busyOff();
                    cb(false);
                });
            }  else if (type == "Edit Address") {
                busyOn('Updating Profile');
                $raysApi.updateShipping(userId, address).then(function(result){
                    if(result.responseCode == 200) {
                        updateSqlite(address);
                        busyOff();
                        cb(true);
                    } else if(result.responseCode == 201) {
                        busyOff();
                        cb(false);
                        $toastMessage.show(result.responseMessage);
                    }
                }, function(error){
                    $toastMessage.show(message.errorMsg);
                    busyOff();
                    cb(false);
                });
            }
        }

        //============================== Add SQLite ============================================ //
                function addSqlite(address) {
                        busyOn("updating");
                        var newAddess = {
                            'addressId'  : address.addressId,
                            'buildingNo': address.buildingNo,
                            'streetNo'  : address.streetNo,
                            'city'      : address.city,
                            'state'     : address.state,
                            'pin_code'  : address.pin_code,
                            'landmark'  : address.landmark
                        };
                        $raysLocalStorage.insertShippingAddress(newAddess).then(function (result) {
                            //getShippingAddr();
                            //$scope.closeModal();
                            busyOff();
                        });

                }
    //============================= Update SQLite ====================================== //
                function updateSqlite(address) {
                    var newAddess = {
                        'addressId'  : address.addressId,
                        'buildingNo': address.buildingNo,
                        'streetNo'  : address.streetNo,
                        'city'      : address.city,
                        'state'     : address.state,
                        'pin_code'  : address.pin_code,
                        'landmark'  : address.landmark
                    };
                    $raysLocalStorage.updateShippingAddress(newAddess).then(function (result) {
                        //getShippingAddr();
                        //$scope.closeModal();
                        busyOff();
                    });
                }
    //============================ Profile Validation Checking ============================== //
            function profileValidation(profileDetails) {
                if(!angular.isDefined(profileDetails.name) || (profileDetails.name == null) || profileDetails.name.length < 1 || (!validateSpecialChar(profileDetails.name))) {
                    return('Please enter your proper Name.');
                } else if(!angular.isDefined(profileDetails.dob) || profileDetails.dob == null) {
                    return('Please enter your Date of Birth.');
                } else if(!validateEmail(profileDetails.email) || (profileDetails.email == null)){
                    return('Invalid email address. Please enter a valid email id.');
                } else if(!angular.isDefined(profileDetails.state) || (profileDetails.state == "null") || (profileDetails.state == null) ) {
                    return('Please enter your State.');
                } else if(!angular.isDefined(profileDetails.city) || (profileDetails.city == "") || (profileDetails.city == null) || (!validateSpecialChar(profileDetails.city))) {
                    return('Please enter your City.');
                } else if(!angular.isDefined(profileDetails.gender) || (profileDetails.gender == "male") || (profileDetails.gender == null) || (profileDetails.gender == "") ) {
                    return('Please enter your Gender.');
                } else if(!angular.isDefined(profileDetails.anniversary) || (profileDetails.anniversary == null) ||  (profileDetails.anniversary == '')) {
                    profileDetails.anniversary   = '';
                } else if(profileDetails.dob.split('.')[2] > profileDetails.anniversary.split('.')[2]) {
                    return('Anniversary Can not be larger than date of birth.');
                }  else if(profileDetails.dob.split('.')[2] == profileDetails.anniversary.split('.')[2]) {
                     if(profileDetails.dob.split('.')[1] > profileDetails.anniversary.split('.')[1]) {
                        return('Anniversary Can not be larger than date of birth.');
                    } else if (profileDetails.dob.split('.')[1] == profileDetails.anniversary.split('.')[1]) {
                        if(profileDetails.dob.split('.')[0] > profileDetails.anniversary.split('.')[0]) {
                            return('Anniversary Can not be larger than date of birth.');
                        } else if (profileDetails.dob.split('.')[0] == profileDetails.anniversary.split('.')[0]) {
                            return('Date of Birth and Date of Anniversary can not be same');
                        }
                    }
                }
            }


    return {
        busyOn              : busyOn,
        busyOff             : busyOff,
        showInfo            : showInfo,
        setData             : setData,
        getData             : getData,
        validateEmail       : validateEmail,
        validateSpecialChar : validateSpecialChar,
        mobileValidate      : mobileValidate,
        pinValidate         : pinValidate,
        checkConnection     : checkConnection,
        hasUserId           : hasUserId,
        getStates           : getStates,
        openModal           : openModal,
        updateShippingAddress : updateShippingAddress,
        addSqlite           : addSqlite,
        updateSqlite        : updateSqlite,
        getGender           : getGender,
        profileValidation   : profileValidation
    }
});
