
var appModule = angular.module('app.Module', [
    'ui.router', 
    'login.Module',
    'dashBoard.Module',
    'advertisement.Module',
    'category.Module',
    'profileInfo.Module',
    'LocalStorageModule',
    'ngDialog'
])

appModule
.run(function() {
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('admin', {
        url         : '/admin',
        abstract    : true,
        templateUrl : 'modules/app/templates/otherTemplate.html',
        controller  : 'appCtrl'
    })
    .state('login', {
        url         : '/login',
        abstract    : true,
        templateUrl : 'modules/app/templates/loginTemplate.html'
    });

    $urlRouterProvider.otherwise('/login/registration');
});
