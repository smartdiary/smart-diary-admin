var loginModule = angular.module('login.Module', []);
loginModule
.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('login.registration', {
        url   : "/registration",
        views : {
            'loginView' : {
                templateUrl : "modules/login/view/registration.html",
                controller  : "RegistrationCtrl"
            }
        }
    });
});
