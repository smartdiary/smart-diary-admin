loginModule
.controller('RegistrationCtrl', function ($scope, $state, localStorageService, $loginService, $timeout) {
	var userId      = localStorageService.get('userId');
    var userName    = localStorageService.get('userName');
    /*****************LogIn Start********************/
    if(userId == null){
        $scope.loginForm = function(form) { 
            $scope.loading = true;
            if ($scope.login_form.$valid) {
                // Submit as normal 
                var loginParam  = {
                        'email'     	: form.username,
                        'password'  	: form.password
                    };
                //console.log(loginParam);

                $loginService.userLogin(loginParam).then(function (regResponseData) {
                    //console.log(regResponseData);
                    if(regResponseData.resCode == 200){
                        var userData = regResponseData.response;
                        localStorageService.set('userId', userData.userTypeId);
                        localStorageService.set('userName', userData.userName);
                        localStorageService.set('userEmail', userData.userEmail);
                        localStorageService.set('userMobile', userData.userMobile);
                        localStorageService.set('companyName', userData.companyName);
                        localStorageService.set('companyAddress', userData.companyAddress);
                        localStorageService.set('userType', userData.userType);
                        
                        
                        $state.go('admin.dash-board');
                        $scope.loading = false;
                    } else if(regResponseData.resCode == 201){
                        $scope.loginMessage = regResponseData.response;
                        form.username = '';
                        form.password = '';
                        
                        $timeout(function () {
                                $scope.loading = false;
                        }, 500);
                    }
                }, function (regResponseData) {
                    console.log('error');
                    $scope.loading = false;
                });
            } else {
                $scope.login_form.submitted = true;
                $timeout(function () {
                        $scope.loading = false;
                }, 500);
            }
        }
    }
    if(userId != null){
        $state.go('admin.dash-board');
    } 
    /***********LogIn end ***************/

    /*****************Registration Start********************/
    if(userId == null){
        $scope.regForm = function(form) { 
            //$scope.loading = true;
            if ($scope.reg_form.$valid) {
                // Submit as normal 
                var regParam  = {
                		'email'			: form.email,
                        'username'     	: form.username,
                        'contactNumber'	: form.contactNumber,
                        'companyName'	: form.companyName,
                        'companyAdd'	: form.companyAdd,
                        'password'  	: form.password,
                        'userType'		: 'advertiser'
                    };
                $loginService.userRegistration(regParam).then(function (regResponseData) {
                    console.log(regResponseData);
                    if(regResponseData.resCode == 200){
                        $scope.regMessage      = regResponseData.response;
                        $scope.loading = false;
                    } else if(regResponseData.resCode == 201){
                        $scope.regMessage      = regResponseData.response;
                        form.email          = '';
                        form.username       = '',
                        form.contactNumber  = '',
                        form.companyName    = '',
                        form.companyAdd     = '',
                        form.password       = '';
                        
                        $timeout(function () {
                                $scope.loading = false;
                        }, 500);
                    } 
                }, function (regResponseData) {
                    console.log('error');
                });
            } else {
                $scope.reg_form.submitted = true;
                $timeout(function () {
                        $scope.loading = false;
                }, 500);
            }
        }
    }
    if(userId != null){
         $state.go('admin.dash-board');
     } 
    /***********Registration end ***************/

    /*****************forgot password Start********************/
    if(userId == null){
        $scope.forgotForm = function(form) { 
            $scope.loading = true;
            if ($scope.forgote_form.$valid) {
                // Submit as normal 
                var forgotParam  = {
                		'email'			: form.email
                    };
                //console.log(forgotParam);
                $loginService.userForgotPassword(forgotParam).then(function (regResponseData) {
                    console.log(regResponseData);
                    if(regResponseData.responseCode == 200){
                        $scope.Message = regResponseData.responseMessage;
                        //$state.go('loginService');
                        $scope.loading = false;
                    } else if(regResponseData.responseCode == 201){
                        $scope.Message = regResponseData.responseMessage;
                        form.email = '';
                        
                        $timeout(function () {
                                $scope.loading = false;
                        }, 500);
                    } 
                }, function (regResponseData) {
                    console.log('error');
                });
            } else {
                $scope.forgote_form.submitted = true;
                $timeout(function () {
                        $scope.loading = false;
                }, 500);
            }
        }
    }
    if(userId != null){
        $state.go('admin.dash-board');
    } 
    /***********forgot password end ***************/
});
