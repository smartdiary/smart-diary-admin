loginModule
.factory('$loginService', function ($http, ENV, $q) {
    function userRegistration(regParam){
        //console.log(regParam);
        var def = $q.defer();
        $http({
            method  : 'POST',
            url     : ENV.apiserverRoot+'/'+ENV.apiUserUrl+'/advertiserRegistration',
            data    : 'userName='+regParam.username+'&userEmail='+regParam.email+'&userMobile='+regParam.contactNumber+'&companyName='+regParam.companyName+'&companyAddress='+regParam.companyAdd+'&userPassword='+regParam.password+'&userType='+regParam.userType,
            headers : {
                        'Content-Type'  : 'application/x-www-form-urlencoded',
                        'apikey'        : 'HTBcrQU49Qf7syks5EUuRCFGv3q2Xkfa9'
                    }
        }).success(function(data) {
            //console.log(data);
            def.resolve(data);
        }).error(function(data){
            //console.log(data);
            def.reject(data);
        }) ;
        return def.promise;
    }
    
    function userLogin(loginParam){
        var def = $q.defer();
        $http({
            method  : 'POST',
            url     : ENV.apiserverRoot+'/'+ENV.apiUserUrl+'/login',
            data    : 'userEmail='+loginParam.email+'&userPassword='+loginParam.password,
            headers : {
                        'Content-Type'  : 'application/x-www-form-urlencoded',
                        'apikey'        : 'HTBcrQU49Qf7syks5EUuRCFGv3q2Xkfa9'
                    }
        }).success(function(data) {
            //console.log(data);
            def.resolve(data);
        }).error(function(data){
            //console.log(data);
            def.reject(data);
        }) ;
        return def.promise;
    }

    function userForgotPassword(forgotParam){
        var def = $q.defer();
        $http({
            method  : 'POST',
            url     : ENV.apiserverRoot+'/'+ENV.apiUserUrl,
            data    : 'email='+forgotParam.email,
            headers : {
                        'Content-Type'  : 'application/x-www-form-urlencoded',
                        'apikey'        : 'HTBcrQU49Qf7syks5EUuRCFGv3q2Xkfa9'
                    }
        }).success(function(data) {
            //console.log(data);
            def.resolve(data);
        }).error(function(data){
            //console.log(data);
            def.reject(data);
        }) ;
        return def.promise;
    }

    return {
        userRegistration    : userRegistration,
        userLogin           : userLogin,
        userForgotPassword  : userForgotPassword
    };
});