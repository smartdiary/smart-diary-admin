profileInfoModule
.controller('ProfileInfoCtrl', function ($scope, $state, $timeout, localStorageService, ngDialog, $profileUpdateService){
    var userId      = localStorageService.get('userId');
    var userName    = localStorageService.get('userName');
    var userEmail    = localStorageService.get('userEmail');
    var userMobile    = localStorageService.get('userMobile');
    var companyName    = localStorageService.get('companyName');
    var companyAddress    = localStorageService.get('companyAddress');
    var userType    = localStorageService.get('userType');

    if(userId != null){
        $scope.username = userName;
        $scope.email = userEmail;
        $scope.userMobile = userMobile;
        $scope.companyName = companyName;
        $scope.companyAddress = companyAddress;
        
        $scope.updateForm = function() { 
            if ($scope.update_form.$valid) {
                var divTemplate = 'Loading. Please wait..';
                ngDialog.open({
                    template: divTemplate,
                    className: 'ngdialog-theme-plain ngdialog-custom',
                    height: "auto",
                    width: "400px",
                    plain: true,
                    overlay: true,
                    closeByEscape: false,
                    closeByDocument: false,
                    closeByNavigation: false,
                    showClose: false,
                    backdrop: 'static',
                    keyboard: false,
                    scope: $scope
                });

                var password = '';
                if($scope.password != null){
                    password = $scope.password;
                } else {
                    password = '';
                }
                
                // Submit as normal 
                var updateParam  = {
                        'userId'        : userId,
                		'email'         : $scope.email,
                        'username'      : $scope.username,
                        'contactNumber' : $scope.userMobile,
                        'companyName'   : $scope.companyName,
                        'companyAdd'    : $scope.companyAddress,
                        'password'      : password,
                        'userType'      : 'advertiser'
                    };
                console.log(updateParam);
                $profileUpdateService.profileUpdate(updateParam).then(function (regResponseData) {
                    console.log(regResponseData);
                    if(regResponseData.responseCode == 200){
                        $state.go('admin.dash-board');
                        ngDialog.close();
                    } else if(regResponseData.responseCode == 201){
                        $scope.Message = regResponseData.responseMessage;
                        form.email = '';
                        form.password = '';
                        
                        $timeout(function () {
                                ngDialog.close();
                        }, 500);
                    } 
                }, function (regResponseData) {
                    console.log('error');
                    ngDialog.close();
                });
            } else {
                $scope.update_form.submitted = true;
                $timeout(function () {
                        ngDialog.close();
                }, 500);
            }
        }
    }
    if(userId == null){
        $state.go('login.registration');
    } 
});
