profileInfoModule
.factory('$profileUpdateService', function ($http, ENV, $q) {
    function profileUpdate(regParam){
        //console.log(regParam);
        var def = $q.defer();
        $http({
            method  : 'POST',
            url     : ENV.apiserverRoot+'/'+ENV.apiUserUrl+'/advertiserRegistration',
            data    : 'userId='+regParam.userId+'&userName='+regParam.username+'&userEmail='+regParam.email+'&userMobile='+regParam.contactNumber+'&companyName='+regParam.companyName+'&companyAddress='+regParam.companyAdd+'&userPassword='+regParam.password+'&userType='+regParam.userType,
            headers : {
                        'Content-Type'  : 'application/x-www-form-urlencoded',
                        'apikey'        : 'HTBcrQU49Qf7syks5EUuRCFGv3q2Xkfa9'
                    }
        }).success(function(data) {
            //console.log(data);
            def.resolve(data);
        }).error(function(data){
            //console.log(data);
            def.reject(data);
        }) ;
        return def.promise;
    }
    
    return {
        profileUpdate    : profileUpdate
    };
});