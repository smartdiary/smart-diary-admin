var profileInfoModule = angular.module('profileInfo.Module', []);
profileInfoModule
.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('admin.userInfo', {
        url   : "/userInfo",
        views : {
            'bodyContent' : {
                templateUrl : "modules/profile/view/view-profile.html",
                controller  : "ProfileInfoCtrl"
            }
        }
    });
});
