var categoryModule = angular.module('category.Module', []);
categoryModule
.config(function ($stateProvider, $urlRouterProvider){
    $stateProvider
    .state('admin.category', {
        url     : "/category",
        views   : {
            'bodyContent' : {
                templateUrl : 'modules/category/view/category.html',
                controller  : 'CategoryController'
            }
        }
    })
    .state('admin.category_add', {
        url     : "/category/add",
        views   : {
            'bodyContent' : {
                templateUrl : 'modules/category/view/add.html',
                controller  : 'CategoryAddController'
            }
        }
    })
});
