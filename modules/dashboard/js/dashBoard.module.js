var dashBoardModule = angular.module('dashBoard.Module', []);
dashBoardModule
.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('admin.dash-board', {
        url   : "/dash-board",
        views : {
            'bodyContent' : {
                templateUrl : "modules/dashboard/view/dashBoard.html",
                controller  : "DashBoardCtrl"
            }
        }
    });
});
