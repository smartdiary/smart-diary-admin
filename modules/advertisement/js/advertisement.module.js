var advertisementModule = angular.module('advertisement.Module', []);
advertisementModule
.config(function ($stateProvider, $urlRouterProvider){
    $stateProvider
    .state('admin.advertisement', {
        url     : "/advertisement",
        views   : {
            'bodyContent' : {
                templateUrl : 'modules/advertisement/view/advertisement.html',
                controller  : 'AdvertisementController'
            }
        }
    })
    .state('admin.add', {
        url     : "/advertisement/add",
        views   : {
            'bodyContent' : {
                templateUrl : 'modules/advertisement/view/add.html',
                controller  : 'AdvertisementAddController'
            }
        }
    })
});
